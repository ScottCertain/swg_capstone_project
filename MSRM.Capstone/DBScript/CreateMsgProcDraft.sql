Create Procedure CreateInquiry
(
	@InquiryId int output,
	@Status nvarchar(50),
	@FirstName nvarchar(25),
	@LastName nvarchar(25),
	@Date date,
	@Message nvarchar(max),
	@EmailAddress nvarchar(50)
)
AS

Insert Into Inquiries (MessageStatus, FirstName, LastName, InquiryDate, Inquiry, EmailAddress)
Values (@Status, @FirstName, @LastName, @Date, @Message, @EmailAddress)
SET @InquiryId = SCOPE_IDENTITY();

GO