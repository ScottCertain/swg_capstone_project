USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[DeleteInquiry]    Script Date: 3/18/2016 11:46:14 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[DeleteInquiry]
(
	@msgId int
)
AS

Delete Inquiries
Where InquiryID = @msgId


GO

