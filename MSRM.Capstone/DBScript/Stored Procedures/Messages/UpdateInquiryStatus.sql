USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[UpdateInquiryStatus]    Script Date: 3/22/2016 10:55:54 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[UpdateInquiryStatus]
( 
	@status bit,
	@InquiryId int
)AS

Update i
Set i.MessageStatus = @Status
From Inquiries i 
Where i.InquiryID = @InquiryId


GO

