USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetAnInquiryById]    Script Date: 3/18/2016 11:46:04 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[GetAnInquiryById]
(
	@msgId int
)
AS

Select i.InquiryID AS MessageId, i.FirstName, i.LastName, i.InquiryDate AS [Date], i.Inquiry AS MessageBody, i.EmailAddress
From Inquiries i
Where i.InquiryID = @msgId


GO

