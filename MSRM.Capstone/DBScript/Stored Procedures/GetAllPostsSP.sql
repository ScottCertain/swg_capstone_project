USE [Capstone]
GO
/****** Object:  StoredProcedure [dbo].[GetAllPosts]    Script Date: 3/14/2016 11:50:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[GetAllPosts]
AS

-- Get all information about a post
SELECT *
FROM Posts p
ORDER BY p.PostDate DESC