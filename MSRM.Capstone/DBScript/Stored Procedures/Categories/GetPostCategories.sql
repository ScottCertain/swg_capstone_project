USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetPostCategories]    Script Date: 3/11/2016 11:15:13 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[GetPostCategories]
(
   @PostId int
) AS

SELECT c.*
FROM PostCategories pc
INNER JOIN Categories c
ON pc.CategoryID = c.CategoryID
WHERE pc.PostID = @PostId

GO

