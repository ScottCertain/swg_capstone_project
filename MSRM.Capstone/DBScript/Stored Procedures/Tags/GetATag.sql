USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetATag]    Script Date: 3/10/2016 10:26:05 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[GetATag]
(
	@Id int
) As

Select t.TagID, t.Tag AS Name
FROM Tags t
Where TagID = @Id


GO

