USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetTagFromTagName]    Script Date: 3/14/2016 9:24:39 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[GetTagFromTagName]
(
	@TagName nvarchar(50)
)AS

Select t.TagID, t.Tag AS Name
From Tags t
Where t.Tag = @TagName


GO

