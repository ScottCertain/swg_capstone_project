USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[GetAllPostTags]    Script Date: 3/15/2016 5:04:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[GetAllPostTags]
(
	@PostId int
)As


SELECT t.TagID, T.Tag AS Name
FROM PostTags pt
INNER JOIN Tags t
ON pt.TagID = t.TagID
WHERE pt.PostID = @PostId




GO

