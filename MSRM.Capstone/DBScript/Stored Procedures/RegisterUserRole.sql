USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[RegisterUserRole]    Script Date: 3/18/2016 3:51:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[RegisterUserRole]
(
	@Email nvarchar(256),
	@Role int
) AS

DELETE FROM AspNetUserRoles
WHERE UserId = (SELECT a.Id
	FROM AspNetUsers a
	WHERE a.Email = @Email)

INSERT INTO AspNetUserRoles
VALUES(
	(SELECT a.Id
	FROM AspNetUsers a
	WHERE a.Email = @Email),@Role)
GO

