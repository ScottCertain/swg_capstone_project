USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[AlterUserInfo]    Script Date: 3/21/2016 3:50:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[AlterUserInfo]
(
	@Email nvarchar(256),
	@UserName nvarchar(256)
) AS

UPDATE AspNetUsers
SET UserName = @UserName
WHERE Email = @Email
GO

