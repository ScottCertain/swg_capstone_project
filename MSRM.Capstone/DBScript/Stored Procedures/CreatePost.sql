USE [Capstone]
GO

/****** Object:  StoredProcedure [dbo].[CreatePost]    Script Date: 3/14/2016 9:31:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[CreatePost](
	@PostID int output,
	@StatusID int,
	@UserID	nvarchar(128),
	@Title nvarchar(255),
	@Posting nvarchar(max),
	@PostDate datetime,
	@PublishDate datetime,
	@ExpireDate datetime
	)
	AS

	INSERT INTO Posts (Title, UserID, StatusID, Post, PostDate, PublishDate, [ExpireDate])
	VALUES (@Title, @UserID, @StatusID, @Posting, @PostDate, @PublishDate, @ExpireDate)
	SET @PostID = SCOPE_IDENTITY();


GO

