﻿namespace MSRM.Capstone.Model
{
    public class Statuses
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}