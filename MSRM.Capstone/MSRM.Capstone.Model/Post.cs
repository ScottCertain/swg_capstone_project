﻿using System;
using System.Collections.Generic;
using System.Security.AccessControl;

namespace MSRM.Capstone.Model
{
    public class Post
    {
        public int PostId { get; set; }
        public int StatusId { get; set; }
        public string UserId { get; set; }
        public string Title { get; set; }
        //Posting = the post // delete this comment
        public string Posting { get; set; }
        public DateTime PostDate { get; set; }  
        public DateTime? PublishDate { get; set; }
        public DateTime? ExpireDate { get; set; }
        public string Notes { get; set; }
        public List<Tag> Tags { get; set; }
        public List<Category> Categories { get; set; }

        public Post()
        {
            StatusId = 1;
            Tags = new List<Tag>();
            Categories = new List<Category>();
        }

        public string DateString => this.PostDate.ToString("D");

        public string GetDateString()
        {
            return this.PostDate.ToString("D");
        }
    }
}
