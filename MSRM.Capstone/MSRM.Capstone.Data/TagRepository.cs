﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using MSRM.Capstone.Data.Settings;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.Data
{
    public class TagRepository
    {
        public List<Tag> _tagList { get; set; }

        public TagRepository()
        {
            _tagList = Load();
        }

        private List<Tag> Load()
        {
            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var tagList = cn.Query<Tag>("GetAllTags",
                    commandType: CommandType.StoredProcedure).ToList();

                return tagList;
            }
        }

        public void AddTag(Tag tagToAdd)
        {

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var t = new DynamicParameters();
                t.Add("TagName", tagToAdd.Name);
                t.Add("TagID", DbType.Int32, direction: ParameterDirection.Output);

                cn.Execute("AddTag", t,
                    commandType: CommandType.StoredProcedure);
                int tagId = t.Get<int>("TagID");
                tagToAdd.TagId = tagId;

                _tagList = Load();

            }
        }

        public Tag GetTagFromName(string name)
        {
            var t = new DynamicParameters();
            t.Add("TagName", name);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var tag = cn.Query<Tag>("GetTagFromTagName", t,
                    commandType: CommandType.StoredProcedure).FirstOrDefault();

                return tag;
            }
        }

        public Tag GetTagFromId(int tagId)
        {
            var t = new DynamicParameters();
            t.Add("Id", tagId);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var tag = cn.Query<Tag>("GetATag", t,
                    commandType: CommandType.StoredProcedure).FirstOrDefault();

                return tag;
            }
        }

        public List<Tag> GetTagsFromPostId(int postId)
        {
            var p = new DynamicParameters();
            p.Add("PostId", postId);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var tagList = cn.Query<Tag>("GetAllPostTags", p,
                    commandType: CommandType.StoredProcedure).ToList();

                return tagList;
            }
        }

        public List<int> GetPostIdsFromTagId(int tagId)
        {
            var p = new DynamicParameters();
            p.Add("TagId", tagId);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                var postList = cn.Query<int>("GetAllTagPostIds", p,
                    commandType: CommandType.StoredProcedure).ToList();

                return postList;
            }
        }

        public void DeleteTag(int tagId)
        {
            var t = new DynamicParameters();
            t.Add("Id", tagId);

            using (SqlConnection cn = new SqlConnection(Settings.Config.ConnectionString))
            {
                cn.Execute("DeleteTag", t,
                    commandType: CommandType.StoredProcedure);
            }

            _tagList = Load();
        }

        public void AddTagsFromPost(int postId, int tagId)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {

                var c = new DynamicParameters();
                c.Add("PostID", postId);
                c.Add("TagID", tagId);

                cn.Execute("AddPostTags", c, commandType: CommandType.StoredProcedure);

            }
        }
    }
}
