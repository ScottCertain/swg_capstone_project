﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using MSRM.Capstone.Data.Settings;
using MSRM.Capstone.Model;
using Dapper;
using System;

namespace MSRM.Capstone.Data
{
    public class PostRepository
    {
        public int Create(Post newPost)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("PostID", DbType.Int32, direction: ParameterDirection.Output);
                parameters.Add("StatusID", newPost.StatusId);
                parameters.Add("UserID", newPost.UserId);
                parameters.Add("Title", newPost.Title);
                parameters.Add("Posting", newPost.Posting);
                parameters.Add("PostDate", newPost.PostDate);
                parameters.Add("PublishDate", newPost.PublishDate);
                parameters.Add("ExpireDate", newPost.ExpireDate);

                cn.Execute("CreatePost", parameters, commandType: CommandType.StoredProcedure);
                int postId = parameters.Get<int>("PostID");
                newPost.PostId = postId;
               
                return postId;
            }
        }

        public void DeleteRelatedCategories(int postId, int categoryId)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("PostId", postId);
                parameters.Add("CategoryId", categoryId);

                cn.Execute("DeletePostCategory", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void DeleteRelatedTags(int postId, int tagId)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("PostId", postId);
                parameters.Add("TagId", tagId);

                cn.Execute("DeletePostTag", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public void Update(Post updatedPost)
        {
            using (SqlConnection cn = new SqlConnection(Config.ConnectionString))
            {
                var parameters = new DynamicParameters();

                parameters.Add("PostId", updatedPost.PostId);
                parameters.Add("StatusId", updatedPost.StatusId);
                //parameters.Add("UserID", updatedPost.UserId);
                parameters.Add("Title", updatedPost.Title);
                parameters.Add("Post", updatedPost.Posting);
                parameters.Add("PostDate", updatedPost.PostDate);
                //parameters.Add("PublishDate", updatedPost.PublishDate);
                //parameters.Add("ExpireDate", updatedPost.ExpireDate);
                parameters.Add("Notes", updatedPost.Notes);
                cn.Execute("UpdatePost", parameters, commandType: CommandType.StoredProcedure);
            }
        }

        public List<Post> GetAllPosts()
        {
            var posts = new List<Post>();

            using (var cn = new SqlConnection(Config.ConnectionString))
            {
                var cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "GetAllPosts",
                    Connection = cn
                };
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        posts.Add(PopulateFromDataReader(dr));
                    }
                }
            }
            return posts;
        }

        public Post GetPostById(int postId)
        {
            var post = new Post();

            using (var cn = new SqlConnection(Config.ConnectionString))
            {
                var cmd = new SqlCommand
                {
                    CommandType = CommandType.StoredProcedure,
                    CommandText = "GetPostById",
                    Connection = cn
                };
                cmd.Parameters.AddWithValue("@PostId", postId);
                cn.Open();

                using (SqlDataReader dr = cmd.ExecuteReader())
                {
                    post = PopulateFromDataReader(dr);
                }
            }
            return post;
        }

        private Post PopulateFromDataReader(SqlDataReader dr)
        {
            var post = new Post();
            post.PostId = (int) dr["PostId"];
            post.Title = dr["Title"].ToString();
            post.PostDate = DateTime.Parse(dr["PostDate"].ToString());
            post.Posting = dr["Post"].ToString();
            post.UserId = dr["UserId"].ToString();
            post.StatusId = (int)dr["StatusId"];

            if (dr["PublishDate"] != DBNull.Value)
            {
                post.PublishDate = DateTime.Parse(dr["PublishDate"].ToString());
            }
            if (dr["ExpireDate"] != DBNull.Value)
            {
                post.ExpireDate = DateTime.Parse(dr["ExpireDate"].ToString());
            }
            
            return post;
        }

    }
}
