﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;
using MSRM.Capstone.UI.Models;

namespace MSRM.Capstone.UI.Controllers
{
    public class PostsController : ApiController
    {
        public NewPostList Get(int id, int size, int status)
        {
            var posts = new PostManager();
            var listOfPosts = posts.GetAllPosts().Data.Where(s => s.StatusId == status).ToList();
            var cats = new CategoryManager();
            var tags = new TagManager();
            var newList = new NewPostList();
            newList.Total = listOfPosts.Count;
            listOfPosts =
                listOfPosts.Skip((id - 1)*size).Take(size).ToList();
            foreach (var post in listOfPosts)
            {
                post.Categories = cats.GetPostCategories(post.PostId).Data;
                post.Tags = tags.GetPostTagsfromPostId(post.PostId).Data;
            }

            newList.Posts = listOfPosts;
           

            return newList;
        }

        public Post GetSinglePost(int id)
        {
            var postManager = new PostManager();
            var cats = new CategoryManager();
            var tags = new TagManager();

            var post = postManager.GetAllPosts().Data.FirstOrDefault(s => s.PostId == id);
            post.Categories = cats.GetPostCategories(post.PostId).Data;
            post.Tags = tags.GetPostTagsfromPostId(post.PostId).Data;

            return post;
        }

        public List<Post> GetPostsByTag(string id)
        {
            var posts = new PostManager();
            var cats = new CategoryManager();
            var tags = new TagManager();
            var idList = tags.GetPostIdsByTag(tags.GetATagFromName(id).Data);
            var listOfPosts = posts.GetAllPosts().Data.Where(s => s.StatusId == 4 && idList.Contains(s.PostId)).ToList();
            

            foreach (var post in listOfPosts)
            {
                post.Categories = cats.GetPostCategories(post.PostId).Data;
                post.Tags = tags.GetPostTagsfromPostId(post.PostId).Data;
            }
            return listOfPosts;
        }

        public List<Statuses> GetStatuses()
        {
           
            var repo = new StatusRepository();

            List<Statuses> statusList = repo.GetAllStatus();

            return statusList;
        } 

        public HttpResponseMessage Post(PostViewModel newPost)
        {
            newPost.Post.UserId = User.Identity.GetUserId();
            newPost.Post.PostDate = DateTime.Now;

            var tags = new TagManager();
            var tagRepo = new TagRepository();
            var posts = new PostManager();
            var response = posts.CreatePost(newPost.Post);

            if (response.Success)
            {
                var categories = new CategoryRepository();
                categories.AddCategoriesFromPost(GetStringArray(newPost.CategoryString), newPost.Post.PostId);

                foreach (string item in GetStringArray(newPost.TagString))
                {
                    Tag newTag = new Tag { Name = item };

                    var tagResponse = tags.CheckForTag(newTag);
                    tagRepo.AddTagsFromPost(newPost.Post.PostId, tagResponse.Data.TagId);
                }

                return Request.CreateResponse(HttpStatusCode.OK, newPost);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Update(PostViewModel updatedPost)
        {
            if (updatedPost.Post.StatusId == 4)
            {
                updatedPost.Post.PostDate = DateTime.Now;
            }
            

            var category = new CategoryManager();
            var tags = new TagManager();
            var tagRepo = new TagRepository();
            var posts = new PostManager();
            var response = posts.Update(updatedPost.Post);

            if (response.Success)
            {
                var postList = tags.GetPostTagsfromPostId(updatedPost.Post.PostId);
                foreach (var item in postList.Data)
                {
                    posts.DeleteRelatedTag(updatedPost.Post.PostId, item.TagId);
                }

                var categoryList = category.GetPostCategories(updatedPost.Post.PostId);
                foreach (var item in categoryList.Data)
                {
                    posts.DeleteRelatedCategory(updatedPost.Post.PostId, item.CategoryId);
                }
                var categories = new CategoryRepository();
                categories.AddCategoriesFromPost(GetStringArray(updatedPost.CategoryString), updatedPost.Post.PostId);
                
                foreach (string item in GetStringArray(updatedPost.TagString))
                {
                    Tag newTag = new Tag { Name = item };

                    var tagResponse = tags.CheckForTag(newTag);
                    tagRepo.AddTagsFromPost(updatedPost.Post.PostId, tagResponse.Data.TagId);
                }

                return Request.CreateResponse(HttpStatusCode.OK, updatedPost);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        private List<string> GetStringArray(string value)
        {
            
            List<string> array = new List<string>();
            if (value != null)
            {
                array = value.Split(',').ToList();
               
            }
            return array;
        }
    }
}
