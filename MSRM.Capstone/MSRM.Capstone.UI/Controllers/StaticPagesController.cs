﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MSRM.Capstone.BLL;
using MSRM.Capstone.UI.Models;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Controllers
{
    public class StaticPagesController : ApiController
    {
        private StaticPagesManager PagesManager { get; set; }

        public StaticPagesController()
        {
            PagesManager = new StaticPagesManager();
        }

        public List<StaticPage> GetAllStaticPages()
        {
            var staticPages = new StaticPagesManager();
            var listOfStaticPages = staticPages.GetAllStaticPages().Data;
            return listOfStaticPages;
        }


        public List<StaticPage> GetPageHeaders()
        {
            var pageHeaders = new StaticPagesManager();
            var listOfPageHeaders = pageHeaders.GetPageHeaders().Data;
            return listOfPageHeaders;
        }


        public StaticPage GetPage(int id)
        {
            var pageManager = new StaticPagesManager();
            var page = pageManager.GetStaticPageById(id).Data;
            return page;
        }

        public HttpResponseMessage Post(StaticPageVm newStaticPage)
        {
            var pages = new StaticPagesManager();
            var response = pages.CreateStaticPage(newStaticPage.StaticPage);

            if (response.Success)
            {
                return Request.CreateResponse(HttpStatusCode.OK, newStaticPage);
            }
            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        [HttpDelete]
        public HttpResponseMessage Delete(int id)
        {
                PagesManager.DeletePage(id);
                return Request.CreateResponse(HttpStatusCode.NoContent);
        }
    }
}
