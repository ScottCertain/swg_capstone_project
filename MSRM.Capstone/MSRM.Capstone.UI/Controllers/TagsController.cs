﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Controllers
{
    public class TagsController : ApiController
    {
        private TagManager _manager { get; set; }

        public TagsController()
        {
            _manager = new TagManager();
        }

        // GET: api/Tags
        public IEnumerable<Tag> Get()
        {
            return _manager.GetAllTags().Data;
        }

        // GET: api/Tags/5
        public Tag Get(int id)
        {
            return _manager.GetATagFromId(id).Data;
        }

        // POST: api/Tags
        public HttpResponseMessage Post([FromBody]Tag newTag)
        {
            if (ModelState.IsValid)
            {
                var response = _manager.AddTag(newTag);
                if (response.Success)
                {
                    var resp = Request.CreateResponse(HttpStatusCode.Created, response.Data);
                    string uri = Url.Link("DefaultApi", new {id = newTag.TagId});
                    resp.Headers.Location = new Uri(uri);

                    return resp;
                }
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, response.Message);
            }
            return Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Invalid Request");
        }

        // DELETE: api/Tags/5
        public HttpResponseMessage Delete(int id)
        {
            if (_manager.GetATagFromId(id).Data != null)
            {
                _manager.DeleteTag(id);
                return Request.CreateResponse(HttpStatusCode.NoContent);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }
        }
    }
}
