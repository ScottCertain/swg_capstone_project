﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Model;
using MSRM.Capstone.UI.Models;

namespace MSRM.Capstone.UI.Controllers
{
    public class AdminController : Controller
	{
        // GET: Admin
        [Authorize(Roles = "SuperUser, User")]
        public ActionResult Index()
		{
            if (User.IsInRole("User"))
            {
                return RedirectToAction("PostManagement");
            }
            else
            {
                return View();
            }
		}

        [Authorize(Roles = "SuperUser")]
        public ActionResult UserManagement()
		{
			return View();
		}

        [Authorize(Roles = "SuperUser")]
        public ActionResult ManagePages()
		{
			List<StaticPage> pages = new List<StaticPage>();

			StaticPage p = new StaticPage();
			p.StaticPageTitle = "TestPage";
			
			pages.Add(p);

			return View(pages);
		}

   
        [HttpPost]
		public ActionResult CreateStaticPage(StaticPage staticPage)
		{
			var pages = new StaticPagesManager();
			var response = pages.CreateStaticPage(staticPage);

			//if (response.Success)
			//{
			//    return View();
			//}

			return View();
		}

        [Authorize(Roles = "SuperUser")]
        public ActionResult CreateStaticPage()
		{
			return View();
		}

        [Authorize(Roles = "SuperUser")]
        public ActionResult EditPage()
		{
				return View();
		}

        [Authorize(Roles = "SuperUser, User")]
        public ActionResult PostManagement()
		{
			return View();
		}
	}
}