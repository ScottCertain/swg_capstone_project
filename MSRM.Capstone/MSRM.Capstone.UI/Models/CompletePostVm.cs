﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Web;
using MSRM.Capstone.BLL;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.UI.Models
{
    public class CompletePostVm
    {
        public Post Post { get; set; }
        public List<Category> Categories { get; set; }
        public List<Tag> Tags { get; set; }


        public CompletePostVm(int PostId)
        {
            var postManager = new PostManager();
            var categoryManager = new CategoryManager();
            var tagManager = new TagManager();
        }
    }
}