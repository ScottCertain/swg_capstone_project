﻿using System;
using System.Linq;
using System.Web;

namespace MSRM.Capstone.UI.Models
{
    public class Pagination
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int PostStatusId { get; set; }
        
    }
}