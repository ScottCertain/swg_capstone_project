﻿var catUri = "/api/Categories/";

$(document).ready(function () {
    $("#btnSubmitCategory").click(function () {
        var category = {};

        category.CategoryName = $("#CategoryName").val();

        $.post(catUri + "POST/", category)
            .done(function () {
                $("#addCategoryModal").modal("hide");
                location.reload();
            })
            .fail(function (jqXhr, status, err) {
                $("#addCategoryModal").modal("hide");
                $("#errorMsg").appendTo("<div class=\"col-xs-offset-1 col-xs-10 alert alert-danger\"><a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" + err + "</div>");
                location.reload();
            });
    });
});