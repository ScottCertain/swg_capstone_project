﻿/// <reference path="../jquery-2.2.1.js" />
var viewMsgUri = '/api/Messages/';

$(document).ready(function () {
    $(document).on("click", ".btnViewDetails", function (e) {
        $('#MessageDetailsModal').modal('show');
        var btn = $(this);


        $.ajax(viewMsgUri + 'GETMSG/' + btn.data("msgId"), {
            method: 'GET'
        }).success(function (msg) {
            $('#msgDetails').empty();
            $(getMessage(msg)).appendTo($('#msgDetails'));

            $(document).on("click", ".btnUpdateMsg", function (e) {
                var updateMsg = {};

                updateMsg.isRead = 1;
                updateMsg.MessageId = msg.MessageId;
                $.ajax({
                    url: '/api/Messages/PUT/' + updateMsg.MessageId,
                    type: 'PUT',
                    dataType: 'json',
                    data: updateMsg,
                    success: function (data) {
                        $('#MessageDetailsModal').modal('hide');
                        $("#msgId" + updateMsg.MessageId + " td:first").text("Read");
                    },
                    error: function (data) { }
                });
            });
        });

    });
});


function getMessage(msg) {
    return '<p>Message Sent From <br> ' + msg.FirstName + ' ' + msg.LastName + '</p>' +
        '<p>Email Address <br>' + msg.EmailAddress + '</p>' +
        '<p>On <br>' + msg.DateString + '</p>' +
        '<p>Message <br>' + msg.MessageBody + '</p>';
};





