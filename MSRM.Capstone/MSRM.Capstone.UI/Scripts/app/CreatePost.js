﻿$(document).ready(function () {
    $('#submitPost').click(function () {
        newPost(4);
        $("#category").tagit("removeAll");
        $("#tag").tagit("removeAll");
    });

    $('#submitReview').click(function () {
        newPost(2);
        $("#category").tagit("removeAll");
        $("#tag").tagit("removeAll");
    });

    $('#draftPost').click(function () {
        newPost(1);
        $("#category").tagit("removeAll");
        $("#tag").tagit("removeAll");
    });

    function newPost(status) {
        var newpost = {};
        var post = {};
        newpost.post = post;

        newpost.post.Title = $('#heading').val();
        newpost.post.Posting = tinyMCE.get('post').getContent();
        newpost.post.StatusId = status;
        newpost.CategoryString = $('#category').val();
        newpost.TagString = $('#tag').val();

       // newPost.post = post;

        var data = {
            id: 1,
            size: 5,
            status: 4
        };

        $.post(postUri + 'Post/', newpost).done(function () {
            $('#heading').val('');
            tinyMCE.activeEditor.setContent('');
            $('#category').val('');
            $('#tag').val('');
            getAllPosts(data);
        })
            .fail(function (jqXhr, status, error) {
                $(errorMessage(error)).appendTo($('#errormsg'));
            });
    }

    function errorMessage(message) {
        return '<div class="alert alert-danger" role="alert" id="danger-alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span></button>' + message + '</div>';
    }


})