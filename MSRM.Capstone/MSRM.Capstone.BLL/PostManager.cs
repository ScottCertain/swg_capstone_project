﻿using System;
using System.Collections.Generic;
using MSRM.Capstone.Data;
using MSRM.Capstone.Model;

namespace MSRM.Capstone.BLL
{
    public class PostManager
    {
        private readonly PostRepository _repo = new PostRepository();

        public Response<List<Post>> GetAllPosts()
        {
            var response = new Response<List<Post>>();
            try
            {
                response.Success = true;
                response.Message = "All posts retrieved from the database.";
                response.Data = _repo.GetAllPosts();
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Failed to load posts.";
                response.Data = new List<Post>();
            }
            return response;
        }

        public Response<Post> GetPostById(int postId)
        {
            var response = new Response<Post>();
            try
            {
                response.Success = true;
                response.Message = "The post has been retrieved.";
                response.Data = _repo.GetPostById(postId);
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Post could not be retrieved.";
                response.Data = new Post();
            }
            return response;
        }

        public Response<Post> CreatePost(Post newPost)
        {
            var response = new Response<Post>();
            try
            {
                response.Success = true;
                response.Message = "Post has been created";
                int newId =_repo.Create(newPost);
                newPost.PostId = newId;
                response.Data = newPost;
            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Failed to create post";
            }

            return response;
        }

        public Response<Post> Update(Post updatedPost)
        {
            var response = new Response<Post>();
            try
            {
                response.Success = true;
                response.Message = "Post has been updated";
                _repo.Update(updatedPost);

            }
            catch (Exception)
            {
                response.Success = false;
                response.Message = "Failed to update post";
            }
            return response;
        }

        public void DeleteRelatedCategory(int postId, int categoryId)
        {
            try
            {
                _repo.DeleteRelatedCategories(postId, categoryId);
            }
            catch (Exception)
            {
                //logg
            }
        }
        
        public void DeleteRelatedTag(int postId, int categoryId)
        {
            try
            {
                _repo.DeleteRelatedTags(postId, categoryId);
            }
            catch (Exception)
            {
                //logg
            }
        }
    }
}
