﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MSRM.Capstone.Data;
using NUnit.Framework;

namespace MSRM.Capstone.Test
{
    [TestFixture]
    class UserTests
    {
        [SetUp]
        public void TestSetUp()
        {
            Utilities.RebuildTestDb();
        }

        [Test]
        public void GetAllUsersTest()
        {
            UserRepository repo = new UserRepository();
            var actual = repo.GetAllUsers();
            Assert.AreEqual(4, actual.Count);
            Assert.AreEqual("mark@markwyrz.com", actual.ToArray()[1].UserName);
        }

        [Test]
        public void ChangeUserRoleTest()
        {
            UserRepository repo = new UserRepository();
            repo.ChangeUserRole("mark@markwyrz.com", 2);
            var actual = repo.GetAllUsers();
            Assert.AreEqual(4, actual.Count);
            Assert.AreEqual(2, actual.ToArray()[1].RoleId);
        }
    }
}
